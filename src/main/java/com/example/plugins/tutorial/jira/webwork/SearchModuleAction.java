package com.example.plugins.tutorial.jira.webwork;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;
//import com.atlassian.sal.api.user.UserManager;
import com.atlassian.jira.user.util.UserManager;

public class SearchModuleAction extends JiraWebActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(SearchModuleAction.class);
    
    @Override
    public String doExecute() throws Exception {
    	if (getHttpRequest().getParameter("key") != null) {
    		return "success"; //je�eli mamy jakikolwiek klucz pokazujemy wynik.
    	}
        return "input"; //widok z samym polem do wpisania klucza.
    }
    
    public String getKey() { //do wykorzystania w widoku
    	if (getHttpRequest().getParameter("key") != null){
    		return getHttpRequest().getParameter("key");
    	}
    	return "";
    }
    
    //tutaj byl myString - po updacie jest zbedny
    
    public String getMyresult() {
    	IssueService issueService = ComponentAccessor.getIssueService();
    	UserManager userManager = ComponentAccessor.getUserManager();
    	ApplicationUser user = userManager.getUserByName(getHttpRequest().getRemoteUser()); 
    	//potrzebujemy usera aby m�c wykona� poni�sz� linijk�, by� mo�e jest jaka� inna metoda ale jeszcze jej nie znalaz�em. bez sztywno zakodowanego admina.
    	final IssueService.IssueResult issueResult = issueService.getIssue(user, getHttpRequest().getParameter("key")); //pobieramy issue = null gdy nie ma b�d� user nie ma uprawnie�.
    	final MutableIssue mutableIssue = issueResult.getIssue();
    	if (mutableIssue != null) {
    		return mutableIssue.getSummary(); //zwracamy summary klucza.
    	}
    	return "Brak Issue dla klucza: " + getHttpRequest().getParameter("key") + "."; //brak klucza b�d� niewystarczaj�ce uprawnienia
    }
}
