package ut.com.example.plugins.tutorial.jira.webwork;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.example.plugins.tutorial.jira.webwork.SearchModuleAction;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class SearchModuleActionTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //SearchModuleAction testClass = new SearchModuleAction();

        throw new Exception("SearchModuleAction has no tests!");

    }

}
